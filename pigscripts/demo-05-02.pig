credit_details = LOAD 'demos/input/input-05-02.txt' AS 
	(name:chararray,
	account:map[],	-- account:map[int] limits value type
	year:int);
-- Look at the structure of the relation.
describe credit_details;
-- Look at the contents of the relation.
dump credit_details;
-- Extract a primitive from the relation.
name_field = FOREACH credit_details GENERATE name;
describe name_field;
dump name_field;
-- Extract a complex type from the relation.
account_field = FOREACH credit_details GENERATE account;
describe account_field;
dump account_field;
-- Extract a member of the complex type from the relation.
limit_field = FOREACH account_field GENERATE account#'creditlimit';
describe limit_field;
dump limit_field;


