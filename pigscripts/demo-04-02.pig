allSales = LOAD 'demos/input/input-04-02.txt' USING PigStorage(',') AS (name, price);
bigSales = FILTER allSales BY price > 2000;		-- in US cents
/*
 * Save the filtered results into a new directory below the
 * directory recognized by the Grunt shell as the working
 * directory.
 */
STORE bigSales INTO 'demos/output/output-04-02';
