stores = LOAD 'demos/input/input-06-07-a.txt' AS (store_id:chararray, name:chararray);
salespeople = LOAD 'demos/input/input-06-07-b.txt' AS (person_id:int, name:chararray, store_id:chararray);
crossed = CROSS stores, salespeople;
DUMP crossed;

