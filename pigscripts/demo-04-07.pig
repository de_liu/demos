allSales = LOAD 'demos/input/input-04-07.txt' USING PigStorage(',') AS (name, price);
bigSales = FILTER allSales BY price > 2000;
STORE bigSales INTO 'demos/output/output-04-07' USING PigStorage(',');
