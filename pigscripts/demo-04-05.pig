people = LOAD 'demos/input/input-04-05.txt' AS (id:int, name:chararray);
noNulls = FILTER people by id IS NOT NULL AND name IS NOT NULL;
STORE noNulls into 'demos/output/output-04-05';

