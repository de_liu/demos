allSales = LOAD 'demos/input/input-04-11.txt' AS (name:chararray, price:float, country:chararray);
sortedSales = ORDER allSales BY country DESC;
STORE sortedSales INTO 'demos/output/output-04-11';
