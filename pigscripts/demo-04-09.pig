allSales = LOAD 'demos/input/input-04-09.txt' AS (name, price, country);
noCountry = FOREACH allSales GENERATE name, price;
STORE noCountry INTO 'demos/output/output-04-09';
