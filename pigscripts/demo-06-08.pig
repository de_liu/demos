june_items = LOAD 'demos/input/input-06-08-a.txt' AS (item:chararray, quantity:int);
july_items = LOAD 'demos/input/input-06-08-b.txt' AS (item:chararray, quantity:int);
both = UNION june_items, july_items;
DUMP both;
