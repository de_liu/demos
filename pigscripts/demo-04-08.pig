lines = LOAD 'demos/input/input-04-08.txt' AS (emailAddress);
emailAddresses = FILTER lines BY emailAddress MATCHES '.+@.+\\.com$';
STORE emailAddresses INTO 'demos/output/output-04-08';
