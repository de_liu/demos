customers =
    LOAD 'demos/input/input-03-01.txt'
    AS (id:int, name:chararray, income:double);
richPeople = FILTER customers BY income > 100000;
STORE richPeople INTO 'demos/output/output-03-01';

