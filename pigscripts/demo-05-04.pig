allSales = LOAD 'demos/input/input-05-04.txt' AS (name:chararray, price:int);
byName = GROUP allSales by name;
totals = FOREACH byName GENERATE group, SUM(allSales.price);
STORE totals INTO 'demos/output/output-05-04';

