allSales = LOAD 'demos/input/input-04-12.txt' AS (name, price, country);
sortedSales = ORDER allSales BY price;
STORE sortedSales INTO 'demos/output/output-04-12';
