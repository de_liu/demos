stores = LOAD 'demos/input/input-06-03-a.txt' AS (store_id:chararray, name:chararray);
salespeople = LOAD 'demos/input/input-06-03-b.txt' AS (person_id:int, name:chararray, store_id:chararray);
joined = JOIN stores BY store_id, salespeople BY store_id;
DESCRIBE joined;
cleaned = FOREACH joined GENERATE 
	stores::store_id, stores::name, person_id, salespeople::name;
STORE cleaned INTO 'demos/output/output-06-03';

