allSales = LOAD 'demos/input/input-05-06.txt' AS (name:chararray, price:int);
byName = GROUP allSales BY name;
separateSales = FOREACH byName GENERATE group, FLATTEN(allSales.price);
STORE separateSales INTO 'demos/output/output-05-06';

