allSales = LOAD 'demos/input/input-05-03.txt' AS (name:chararray, price:int);
byName = GROUP allSales BY name;
describe byName;
STORE byName INTO 'demos/output/output-05-03';

