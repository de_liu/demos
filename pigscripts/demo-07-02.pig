define calc_commission(NAME, SPLIT_AMT, LOW_PCT, HIGH_PCT)
returns result {
	allSales = LOAD 'demos/input/input-07-02.txt' AS (name, price);
	byperson = FILTER allSales by name == '$NAME';
	SPLIT byperson INTO low if price < $SPLIT_AMT,
		high if price >= $SPLIT_AMT;
	amt1 = FOREACH low GENERATE name, price * $LOW_PCT as amount;
	amt2 = FOREACH high GENERATE name, price * $HIGH_PCT as amount;
	commissions = UNION amt1, amt2;
	grouped = GROUP commissions BY name;
	$result = FOREACH grouped GENERATE group, SUM(commissions.amount);
};

alice_comm = calc_commission('Alice', 1000, 0.07, 0.12);
dump alice_comm;
carlos_comm = calc_commission('Carlos', 2000, 0.08, 0.14);
dump carlos_comm;
