stores = LOAD 'demos/input/input-06-01-a.txt' AS (store_id:chararray, name:chararray);
salespeople = LOAD 'demos/input/input-06-01-b.txt' AS (person_id:int, name:chararray, store_id:chararray);
grouped = COGROUP stores BY store_id, salespeople BY store_id;
DUMP grouped;

