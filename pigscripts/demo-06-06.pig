stores = LOAD 'demos/input/input-06-06-a.txt' AS (store_id:chararray, name:chararray);
salespeople = LOAD 'demos/input/input-06-06-b.txt' AS (person_id:int, name:chararray, store_id:chararray);
joined = JOIN stores BY store_id FULL OUTER, salespeople BY store_id;
DUMP joined;

