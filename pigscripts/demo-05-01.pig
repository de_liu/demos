sales_details = LOAD 'demos/input/input-05-01.txt' 
	AS (trans_id:chararray, amount:int, items_sold:bag
		{item:tuple(SKU:chararray, price:int)});
-- Look at the structure of the relation.
describe sales_details;
-- Look at the contents of the relation.
dump sales_details;
-- Extract a primitive from the relation.
trans_id_field = FOREACH sales_details GENERATE trans_id;
describe trans_id_field;
dump trans_id_field;
-- Extract a complex type from the relation.
item_field = FOREACH sales_details GENERATE items_sold;
describe item_field;
dump item_field;
-- Extract a member of the complex type from the relation.
SKU_field = FOREACH item_field GENERATE items_sold.SKU;
describe SKU_field;
dump SKU_field;


