employees = LOAD 'demos/input/input-05-07.txt' AS (title:chararray, salary:int);
title_groups = GROUP employees BY title;
top_salaries = FOREACH title_groups {
	sorted = ORDER employees BY salary DESC;	-- employees is bag name.
	highest_paid = LIMIT sorted 3;
	GENERATE group, highest_paid;
};
STORE top_salaries INTO 'demos/output/output-05-07';
