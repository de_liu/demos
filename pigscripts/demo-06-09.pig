customers = LOAD 'demos/input/input-06-09.txt' AS (name:chararray, ltv:int); 
SPLIT customers INTO
	gold_program IF ltv >= 25000,
	silver_program IF ltv >= 10000 AND ltv < 25000;
DUMP gold_program;
DUMP silver_program;

