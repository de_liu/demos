#!/bin/sh

cmdArgs=$#
args=""
unit=""
demo=""

if [ $cmdArgs -gt 2 ]
then

# Read the command line arguments.

    unit=$1
    demo=$2

    shift
    shift
    for var in $* 
    do
        args="$args -p $var "
        shift
    done
elif [ $cmdArgs -lt 2 ]
then
    echo "Format  :	./runPigDemo.sh UNIT DEMO"
    echo "Example :	./runPigDemo.sh 03 01"
    exit 1
fi

# Get the working directory.

original=`pwd`

# Go to the parent of the demo directory

cd ../..

# Remove previous data

hadoop fs -rm -r "demos/output/output-$unit-$demo" >/dev/null 2>&1
 
# Show input

echo "===== Input Data ======"
hadoop fs -cat "demos/input/input-$unit-$demo*.txt"
echo 
echo "======================="

# Process data

echo "===== Dumped Data ====="

pig -4 "$original/nolog.conf" $args "demos/pigscripts/demo-$unit-$demo.pig" 
echo "======================="

# Show output

echo "===== Stored Data ====="
hadoop fs -cat "demos/output/output-$unit-$demo/part-[mr]-00000" 2>/dev/null
echo "======================="

exit

# Return to the original directory.

cd $original

# Terminate the script.

exit 0

