#!/bin/sh

if [ $# -ne 2 ]
then
    echo "Format  :	./runLocalPigDemo.sh UNIT DEMO"
    echo "Example :	./runLocalPigDemo.sh 03 01"
    exit 1
fi

# Read the command line arguments.

UNIT=$1
DEMO=$2

# Get the working directory.

ORIGINAL=`pwd`

# Go to the parent of the demo directory

cd ../..

# Remove previous data

rm -rf "demos/output/output-$UNIT-$DEMO" >/dev/null 2>&1

# Show input

echo "===== Input Data ======"
cat "demos/input/input-$UNIT-$DEMO.txt"
echo
echo "======================="

# Process data

echo "===== Dumped Data ====="
pig -4 "$ORIGINAL/nolog.conf" -x local "demos/pigscripts/demo-$UNIT-$DEMO.pig"
echo "======================="

# Show output

echo "===== Stored Data ====="
cat "demos/output/output-$UNIT-$DEMO/part-[mr]-00000" 2>/dev/null
echo "======================="

# Return to the original directory.

cd $ORIGINAL

# Terminate the script.

exit 0

