#!/bin/sh

# To install the demo data:
# Copy the demo directory (containing this file) onto the linux
# VM desktop. Then, from the demo directory, type the following
# command: ./setup.sh

# Remove any previous remnants of the demo directory.
hadoop fs -rm -r demos >/dev/null 2>&1

# Copy the demos into hdfs.
echo "Copying demos directory into HDFS ..."
hadoop fs -put ../../demos demos

# Terminate the script.
echo "Installation Complete"
exit 0

